FROM registry.gitlab.com/adrift/games/docker-steamcmd

USER root
RUN apt-get update                              && \
    apt-get install libstdc++6:i386                \
                    libcurl4-gnutls-dev:i386 -y && \
    apt-get clean                               && \
    rm -rf /var/lib/apt/lists/*
    
WORKDIR /opt/steam/garrysmod   

ADD garrysmod/files/mount.cfg ./garrysmod/cfg/mount.cfg
ADD garrysmod/files/start.sh .

RUN chmod +x start.sh         && \
    mkdir -p /opt/steam       && \
    chown -R steam /opt/steam

USER steam

# gmod, tf2, css
RUN steamcmd +login anonymous +force_install_dir /opt/steam/garrysmod +app_update 4020 validate +quit && \
    steamcmd +login anonymous +force_install_dir /opt/steam/css +app_update 232330 validate +quit

ENTRYPOINT [ "./start.sh" ]