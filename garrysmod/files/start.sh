#!/bin/bash

#main stuffs
[[ -z $SV_HOSTNAME ]] && SV_HOSTNAME="Garry's Mod | gitlab.com/adrift/games/docker-garrysmod"
[[ -n $SV_PASSWORD ]] && SV_PASSWORD="$SV_PASSWORD"
[[ -n $RCON_PASSWORD ]] && RCON_PASSWORD="$RCON_PASSWORD"
[[ -n $HOST_WORKSHOP_COLLECTION ]] && HOST_WORKSHOP_COLLECTION="$HOST_WORKSHOP_COLLECTION"
[[ -z $MAP ]] && MAP="gm_construct"
[[ -z $GAMEMODE ]] && GAMEMODE="sandbox"
[[ -z $SV_LAN ]] && SV_LAN="0"
[[ -z $MAXPLAYERS ]] && MAXPLAYERS="20"
[[ -z $SRCDS_PORT ]] && PORT="27015"

#network
[[ -n $SV_DOWNLOADURL ]] && SV_DOWNLOADURL="$SV_DOWNLOADURL"
[[ -z $SV_LOADINGURL ]] && SV_LOADING_URL="https://adrift.io"
[[ -z $NET_MAXFILESIZE ]] && NET_MAXFILESIZE="64"
[[ -z $SV_MINRATE ]] && SV_MINRATE="7500"
[[ -z $SV_MAXRATE ]] && SV_MAXRATE="0"
[[ -z $SV_MINUPDATERATE ]] && SV_MINUPDATERATE="33"
[[ -z $SV_MAXUPDATERATE ]] && SV_MAXUPDATERATE="66"
[[ -z $SV_MINCMDRATE ]] && SV_MINCMDRATE="33"
[[ -z $SV_MAXCMDRATE ]] && SV_MAXCMDRATE="66"

#game settings
[[ -z $SV_AIRACCELERATE ]] && SV_AIRACCELERATE="100"
[[ -z $SV_GRAVITY ]] && SV_GRAVITY="600"
[[ -z $SV_ALLOW_WAIT_COMMAND ]] && SV_ALLOW_WAIT_COMMAND="0"
[[ -z $SV_ALLOW_VOICE_FROM_FILE ]] && SV_ALLOW_VOICE_FROM_FILE="0"
[[ -z $SV_TURBOPHYSICS ]] && SV_TURBOPHYSICS="0"
[[ -z $SV_MAX_USERCMD_FUTURE_TICKS ]] && SV_MAX_USERCMD_FUTURE_TICKS="12"
[[ -z $GMOD_PHYSITERATIONS ]] && GMOD_PHYSITERATIONS="4"
[[ -z $SV_CLIENT_MIN_INTERP_RATIO ]] && SV_CLIENT_MIN_INTERP_RATIO="1"
[[ -z $SV_CLIENT_MAX_INTERP_RATIO ]] && SV_CLIENT_MAX_INTERP_RATIO="2"
[[ -z $THINK_LIMIT ]] && THINK_LIMIT="20"
[[ -z $SV_REGION ]] && SV_REGION="0"
[[ -z $SV_NOCLIPSPEED ]] && SV_NOCLIPSPEED="5"
[[ -z $SV_NOCLIPACCELERATE ]] && SV_NOCLIPACCELERATE="5"
[[ -z $SV_LAN ]] && SV_LAN="0"
[[ -z $SV_ALLTALK ]] && SV_ALLTALK="1"
[[ -n $SV_CONTACT ]] && SV_CONTACT="$SV_CONTACT"
[[ -z $SV_CHEATS ]] && SV_CHEATS="0"
[[ -z $SV_ALLOWCSLUA ]] && SV_ALLOWCSLUA="0"
[[ -z $SV_PAUSABLE ]] && SV_PAUSABLE="0"
[[ -z $SV_FILTERBAN ]] && SV_FILTERBAN="1"
[[ -z $SV_FORCEPRELOAD ]] && SV_FORCEPRELOAD="1"
[[ -z $SV_FOOTSTEPS ]] && SV_FOOTSTEPS="1"
[[ -z $SV_VOICEENABLE=1 ]] && SV_VOICEENABLE="1"
[[ -z $SV_VOICECODEC ]] && SV_VOICECODEC="vaudio_speex"
[[ -z $SV_TIMEOUT ]] && SV_TIMEOUT="120"
[[ -z $SV_DELTAPRINT ]] && SV_DELTAPRINT="0"
[[ -z $SV_ALLOWUPLOAD ]] && SV_ALLOWUPLOAD="0"
[[ -z $SV_ALLOWDOWNLOAD ]] && SV_ALLOWDOWNLOAD="0"

#sandbox settings
[[ -z $SBOX_NOCLIP  ]] && SBOX_NOCLIP="1"
[[ -z $SBOX_GODMODE ]] && SBOX_GODMODE="0"
[[ -z $SBOX_WEAPONS ]] && SBOX_WEAPONS="0"
[[ -z $SBOX_PLAYERSHURTPLAYERS ]] && SBOX_PLAYERSHURTPLAYERS="1"
[[ -z $SBOX_MAXPROPS ]] && SBOX_MAXPROPS="100"
[[ -z $SBOX_MAXRAGDOLLS ]] && SBOX_MAXRAGDOLLS="25"
[[ -z $SBOX_MAXNPCS ]] && SBOX_MAXNPCS="25"
[[ -z $SBOX_MAXBALLOONS ]] && SBOX_MAXBALLOONS="25"
[[ -z $SBOX_MAXEFFECTS ]] && SBOX_MAXEFFECTS="15"
[[ -z $SBOX_MAXDYNAMITE ]] && SBOX_MAXDYNAMITE="0"
[[ -z $SBOX_MAXLAMPS ]] && SBOX_MAXLAMPS="5"
[[ -z $SBOX_MAXTHRUSTERS ]] && SBOX_MAXTHRUSTERS="20"
[[ -z $SBOX_MAXWHEELS ]] && SBOX_MAXWHEELS="20"
[[ -z $SBOX_MAXHOVERBALLS ]] && SBOX_MAXHOVERBALLS="20"
[[ -z $SBOX_MAXVEHICLES ]] && SBOX_MAXVEHICLES="3"
[[ -z $SBOX_MAXBUTTONS ]] && SBOX_MAXBUTTONS="20"
[[ -z $SBOX_MAXEMITTERS ]] && SBOX_MAXEMITTERS="5"

cat <<EOF >/opt/steam/garrysmod/garrysmod/cfg/server.cfg
hostname $SV_HOSTNAME
rcon_password $RCON_PASSWORD
sv_password $SV_PASSWORD
log on
sv_logbans 1
sv_logecho 1
sv_logfile 1
sv_log_onefile 0
lua_log_sv 0
sv_downloadurl $SV_DOWNLOADURL
sv_loadingurl $SV_LOADINGURL
net_maxfilesize $NET_MAXFILESIZE
sv_maxrate $SV_MAXRATE
sv_minrate $SV_MINRATE
sv_maxupdaterate $SV_MAXUPDATERATE
sv_minupdaterate $SV_MINUPDATERATE
sv_maxcmdrate $SV_MAXCMDRATE
sv_mincmdrate $SV_MINCMDRATE
sv_airaccelerate $SV_AIRACCELERATE
sv_gravity $SV_GRAVITY
sv_allow_wait_command $SV_ALLOW_WAIT_COMMAND
sv_allow_voice_from_file $SV_ALLOW_VOICE_FROM_FILE
sv_turbophysics $SV_TURBOPHYSICS
sv_max_usercmd_future_ticks $SV_MAX_USERCMD_FUTURE_TICKS
gmod_physiterations $GMOD_PHYSITERATIONS
sv_client_min_interp_ratio $SV_CLIENT_MIN_INTERP_RATIO
sv_client_max_interp_ratio $SV_CLIENT_MAX_INTERP_RATIO
think_limit $THINK_LIMIT
sv_region $SV_REGION
sv_noclipspeed $SV_NOCLIPSPEED
sv_noclipaccelerate $SV_NOCLIPACCELERATE
sv_lan $SV_LAN
sv_alltalk $SV_ALLTALK
sv_contact $SV_CONTACT
sv_cheats $SV_CHEATS
sv_allowcslua $SV_ALLOWCSLUA
sv_pausable $SV_PAUSABLE
sv_filterban $SV_FILTERBAN
sv_forcepreload $SV_FORCEPRELOAD
sv_footsteps $SV_FOOTSTEPS
sv_voiceenable $SV_VOICEENABLE
sv_voicecodec $SV_VOICECODEC
sv_timeout $SV_TIMEOUT
sv_deltaprint $SV_DELTAPRINT
sv_allowupload $SV_ALLOWUPLOAD
sv_allowdownload $SV_ALLOWDOWNLOAD
sbox_noclip $SBOX_NOCLIP
sbox_godmode $SBOX_GODMODE
sbox_weapons $SBOX_WEAPONS
sbox_playershurtplayers $SBOX_PLAYERSHURTPLAYERS
sbox_maxprops $SBOX_MAXPROPS
sbox_maxragdolls $SBOX_MAXRAGDOLLS
sbox_maxnpcs $SBOX_MAXNPCS
sbox_maxballoons $SBOX_MAXBALLOONS
sbox_maxeffects $SBOX_MAXEFFECTS
sbox_maxdynamite $SBOX_MAXDYNAMITE
sbox_maxlamps $SBOX_MAXLAMPS
sbox_maxthrusters $SBOX_MAXTHRUSTERS
sbox_maxwheels $SBOX_MAXWHEELS
sbox_maxhoverballs $SBOX_MAXHOVERBALLS
sbox_maxvehicles $SBOX_MAXVEHICLES
sbox_maxbuttons $SBOX_MAXBUTTONS
sbox_maxemitters $SBOX_MAXEMITTERS
exec banned_user.cfg
exec banned_ip.cfg
heartbeat
EOF

steamcmd +login anonymous +force_install_dir /opt/steam/garrysmod +app_update 4020 validate +quit && \
steamcmd +login anonymous +force_install_dir /opt/steam/css +app_update 232330 validate +quit
    
if [ -z "$HOST_WORKSHOP_COLLECTION" ]
then
    exec ./srcds_run -game garrysmod +sv_lan $SV_LAN +ip 0.0.0.0 +map $MAP +gamemode $GAMEMODE +host_workshop_collection $HOST_WORKSHOP_COLLECTION -usercon -port $SRCDS_PORT -maxplayers $MAXPLAYERS -secure $OTHER_ARGS $@
else
    exec ./srcds_run -game garrysmod +sv_lan $SV_LAN +ip 0.0.0.0 +map $MAP +gamemode $GAMEMODE -usercon -port $SRCDS_PORT -maxplayers $MAXPLAYERS -secure $OTHER_ARGS $@
fi